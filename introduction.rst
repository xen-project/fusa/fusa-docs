..

############
Introduction
############

This is an example page for the Xen |Xen version| FuSa documentation, here a
quick substitution is used to generate the Xen version.
We can have also a quick link to the website here: `XenWebsite`_

.. note::
  in conf.py, the `rst_prolog` variable is linked to the
  sphinx_substitution_extensions extension.


.. _introduction_to_the_docs:

********
Examples
********

There is a link to the introduction
:ref:`Intro <introduction_to_the_docs>`.


Section
=======

This is another section containing one image:

|

  **Image**

    Something about this image.

.. image:: images/xen_logo.svg
   :align: center
